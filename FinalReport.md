# 2048 (GUI) Final Report


## Code Repository

https://TD-Y@bitbucket.org/TD-Y/2048.git

## Group Members

* Octavio E. Lobato-Buendia


## Final Milestone

The Program has came a long way since the Intermediate Report. There was hardly any code written
when the Intermediate Report was submitted. Looking at the code implemented now, there is a substantual
amount of implemented code. There was also a freetype error that was fixed by adding a two lines pointing
to the freetype dylib in the cargo config file.


## Accomplishments

Although the game itself is far from finished, I beleive there are a good amount of noteworthy accomplishments
for this project. I pretty much ended up re-making the 2048 game. What I mean by this is from the provided skeleton
project the only feature I used was the Seed::new() method to obtain the randomness of the game. Not using most of
the skeleton project made me create my own logic for the grid, a 16 length vector. This vector is filled with tiles
that have a value of zero, this tells the logic that the index of the tile, or cell of grid, is empty.

An interesting algorithm I created to was how to find the column and row of a tile based only from the index of the tiles
vector. It seemed difficult at first but by using the index mod 4 always
resulted in the column index. As for the row I was able to use the floor of the index divided by 4. This lead me
to the algorithm of how to determine which empty cells the tiles can move to with user input. It's as simple as looping
once through the tiles vector. The direction up and down are the same however up loops from 4, skipping the first row,
to 15, while down loops from 11, skipping the bottom row, to 0. The reasoning behind this is the tiles closest to the
direction of the key press must update their positions first for accurate changes. An example of how the up key works:
whenever a non empty cell, a tile with a value greater than 0, appears in the loop the logic goes through another small
loop depending on row the tile is in. So if a tile exists at index 10 it lies in row 2, rows are from 0 to 3.
The logic then knows it needs to check two cells above the tile, cell 6 then cell 2. If no tiles occupy cell 2 then
the tile originally at cell 10 will swap indexes with cell 2. Likewise if there is a tile in cell 6 but not cell 2,
the logic whould have moved the tile in cell 6 to cell 2 before encountering the tile at cell 10, the logic would thus move the
tile to cell 6, or index 6.


## Works in Progress

There is still a lot of work left to be done. The fact that I was working alone while having to deal with other classes
homework, tests, and finals should demenstrait the very little time I had to work on this project. There were also a lot
of setbacks caused by the poor documentation of the pistion library and its backend libraries, as well as technical issues.
It was hard to determine what some methods did when most of them dont explain what and how they work. The most frustrating
issue was the incorrect docs posted online. For example the repo of the graphics crate had a link to its online docs, in
this doc it contained a method called default_draw_state for rendering that required contexts, however, I would call this
method and receive a method not found error. After digging around I cloned the graphics crate, opened its docs from my
computer and found that that method was not present. This and many other misleading docs set me back a lot.

The features that I still need to implement are the left down and right movments of the tiles. I have implemented the
up so the others should be really easy, I just couldn't implement them in time. I also need to implement the merging
of tiles, score, animations, and end game results.


## Technical Challenges

Setting up freetype was one of the most frustrating things to happen especially because there is very little
help on the internet for a not well known project. I followed the pistion tutorials where all they had you do was
run "brew install freetype" if you had homebrew on your computer. Turns out I had homebrew, it downloaded and the program failed to
run because it was missing freetype. After digging around a lot on the internet I was able to find a solution where
I create a config file in my .cargo directory. In the config file I add the following lines:
"[target.x86_64-apple-darwin.freetype]" and "rust-link-lib = 'freetype:dylib'" this pointed cargo to the freetype
library. This fix worked perfectly until I implemented GlyphCache to display text. After implementing GlyphCache I
started receiving an error that described FT classes not available for x86_64 architecture. I once again snooped
around the internet with no luck. I tried to find the reason why some people only had to simply run brew install freetype
and found that I also needed to run "brew install pkg-config" which automatically placed freetype in its correct location
for the cargo compiler. After removing the previously created config file I was able to code freely with no more
freetype issues.
