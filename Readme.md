# 2048 (GUI) Proposal

**Author:** Octavio E. Lobato-Buendia  
**Class:** CS:3820 Programming Language Concepts  
**Intermediate Milestone:** April 17th, 2016  
**Final Milestone:** May 9th, 2016

**Note:** This proposal is an overview of what should be complete by the  
intermediate milestone date and the final milestone date. Each goal will  
have a description explaining the goal in detail.

## [Project](https://bitbucket.org/AdrienChampion/plc_final_project/src/#markdown-header-2048-gui)

Design a Graphical User Interface (GUI) for the game 2048 using
[Pistion](https://github.com/PistonDevelopers/piston).


## Milestone Goals

1. [Complete Overview of the Piston Library](#markdown-header-complete-overview-of-the-piston-library)
2. [Project Structure](#markdown-header-project-structure)
3. [Graphic Assets](#markdown-header-graphic-assets)
4. [GUI Design](#markdown-header-gui-design)
5. [Code](#markdown-header-code)

## Final Goals

1. [Complete Game](#markdown-header-complete-game)
2. [Presentation](#markdown-header-presentation)


## Milestone Goals In Detail

### Complete Overview of the Piston Library

Fully understand the Piston Library to be able to code the GUI with ease.  
This allows the coding process to run much smoother, no need to open and  
close Piston docs.


### Project Structure

Have a **full layout of the project architecture**. This will keep everything  
clean so the project does not turn into a hierarchy of unnecessary rust  
files, nor contain spaghetti code. This will make future development much  
more efficient by making the project easy to return to, as well as make the  
project easy to understand by others who have never viewed the original code.  


### Graphic Assets

These are all the assets required for the creation of each GUI interface.  
The assets will consist of PNG files that are renderable. No need to worry  
about the colors of each file since that will be handled by [Pistion](https://github.com/PistonDevelopers/piston).

1. Global PNG files of renderable text
	* Numbers 0 to 9
	* “Quit” - Button

#### Menu

The main menu of 2048. The player will always land on this GUI interface  
at the startup of the program. The player will also be returned to this  
interface when they win or lose. This interface includes the **logo**, a **play  
button**, a **quit button**, and an **instructions button**.

1. PNG files of renderable text
	* “Play” - Button
	* “Instructions” - Button

#### Instructions

This GUI interface will display the instructions of 2048 along with  
the credits. This includes how to **win**, **lose**, **2048’s rules**, a **back  
button**, and the **credits**.

1. PNG files of renderable text
	* Instruction text
	* Credit text
	* “Back” - Button

#### Game View

This GUI interface will display the game view of 2048. This includes the  
players **score**, the **4x4 board**, a **pause button**, a **quit button**, and the **tiles**.

1. PNG files of renderable text
	* Tiles - Holds the numbers
	* “Pause” -Button


### GUI Design

Three drawn out layouts in color of each GUI interface; the **menu**,  
**instructions**, and the **game view**. This design is not final, but will be  
used until the completion of the project.


### Code

Fully coded the **menu**, and the **instructions** interfaces. This means a user  
will actually be able to interact with the menu and the instructions.


## Final Goals in Detail

### Complete Game

Fully working version of the entire game. This includes a fully functional  
**menu**, **instructions**, and **game** interface.


### Presentation

A complete presentation of the entire project designed for a presentation  
to the class. This will include my **process of work**, **concepts relating back  
to class**, **encountered problems**, and the **game** itself.