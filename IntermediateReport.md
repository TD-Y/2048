# 2048 (GUI) Intermediate Report

**Author** Octavio E. Lobato-Buendia  
**Class** CS:3820 Programming Language Concepts  
**Final Milestone** May 9th, 2016  


## Milestone Goals Complete

* [Project Structure](#markdown-header-project-structure)
* [GUI Design](#markdown-header-gui-design)

## Milestone Goals Partially Complete

* [Complete Overview of the Piston Library](#markdown-header-complete-overview-of-the-piston-library)
* [Graphic Assets](#markdown-header-graphic-assets)

## Milestone Goals Not Complete

* [Code](#markdown-header-code)


## Milestone Goals Complete In Detail

### Project Structure

Completed the project structure shown below. Under src there is another  
folder called view. Inside view will be all of the structs to handle  
each view of 2048. Inside src/assets you will find the renderer struct  
used to render png files that require cutting, see the [design](#markdown-header-gui-design)  
for more details. Inside the bin folder you will find an assets folder  
that contains each png file for rendering the text. The titles are pretty  
self explanatory.

1. 2048
    1. src
        1. view
            * menu.rs
            * help.rs
            * game.rs
            * mod.rs
        2. assets
            * renderer.rs
        3. settings.rs
        4. main.rs
    2. bin
        1. assets
            * play.png
            * help.png
            * back.png
            * newGame.png
            * score.png
            * giveUp.png
            * numbers.png
            * howTo.png
    3. Cargo.lock
    4. Cargo.toml


### GUI Design
Obviously the final product wont look as bad as my poorly drawn desing, I  
will be using an image design program. There are a few changes in the design  
since the proposal. First I only created a design without color. The reasoning  
behind this is because the colors are something to play around with in the code.  
Second the changes consist of a few button text changes.  

![GUI DESIGN 1](guiDesign1.png)
![GUI DESIGN 2](guiDesign2.png)


## Milestone Goals Partially Complete

### Complete Overview of the Piston Library

Piston has a huge library and some of the docs are a bit hard to interpret. See  
the [Code](#markdown-header-code) section for details.  


### Graphic Assets

This aspect of the project was a bit difficult for me since I'm definitely no artist.  
Luckily I have worked with a few image editing programs in the past that helped me  
remove the backgrounds of each image, as well as cropping.  

**note:** There are a few changes to the png files since the proposal.  
There is no longer a quit png file, the instructions text has been  
changed to help, the pause has been removed completely since it is  
redundant in a timeless game of 2048, and the instructions/credits  
text will be rendered as ordinary text instead of big 2D text.  

1. Completed assets
    * play.png
    * back.png
    * howTo.png

2. Incomplete Assets
    * newGame.png
    * score.png
    * giveUp.png
    * numbers.png


## Milestone Goals Not Complete

### Code

First let me begin by saying the past two weeks have given me almost no time  
to work on this project. Having exam after exam and another programming assignment  
I had to work on, [led_httpserver](https://bitbucket.org/TD-Y/led_httpserver/src).


What affected me the most was a freetype error. Even though I have freetype installed  
on my machine none of my code would run because it was missing a freetype library. This  
was very unfortunate because I would spend hours trying to figure out what was wrong with  
no success. This kept me from getting a nice overview of the piston library since I could  
not implement example projects for learning purposes.  