// Piston and back-end libraries.
use piston::window::WindowSettings;
use opengl_graphics::OpenGL;
use glutin_window::GlutinWindow;
// Project libraries
use math::Size;

/// App settings.
pub struct Settings {
    /// The size of the window
    window_size: Size,
}
impl Settings {
    /// Creates a new Settings.
    pub fn new(size: Size) -> Settings {
        Settings {
            window_size: size,
        }
    }

    /// Builds the window with the provided settings.
    pub fn build_window(&self) -> GlutinWindow {
        // Return created window
        let mut window: GlutinWindow = WindowSettings::new("2048", [self.window_size.width as u32, self.window_size.height as u32])
                            .opengl(OpenGL::V3_2)
                            .exit_on_esc(true)
                            .build()
                            .unwrap();
        window
    }

    /// Get the window size.
    pub fn window_size(&self) -> &Size {
        &self.window_size
    }
}

