// Adrien Champion crate
extern crate lib_2048 ;
// Piston and backed crates
extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;
// Rust crates
extern crate rand;

// Adrien Champion lib
pub use lib_2048::{ Seed, Grid, Dir, Evolution } ;
// Piston core
use piston::window::WindowSettings;
use piston::event_loop::*;
// Piston input
use piston::input::Event::{Render, AfterRender, Update, Idle, Input};
use piston::input::Input::Press;
use piston::input::Button::Keyboard;
use piston::input::keyboard::Key::{Up, Down, Left, Right};
// Back-end graphics
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{ GlGraphics, OpenGL };
// Project library
use menu::Menu;
use game::Game;
use settings::Settings;
use view::View;
use math::Size;

// Project modules
mod component;
mod tile;
mod math;
mod color;
mod menu;
mod game;
mod settings;
mod view;

fn main() {
    use view::View::*;

    // Set settings
    let settings = Settings::new(Size::new(350.0, 500.0));
    let mut window = settings.build_window();

    // Initialize all views, this way you don't have to rebuild each GUI view.
    let mut gl = GlGraphics::new(OpenGL::V3_2);

    let view_type_menu = MENU(Menu::new());
    let view_type_game = GAME(Game::new(settings.window_size(), &mut gl));
    let view_type_instructions = INSTRUCTIONS;

    // Set our current view to the default menu view.
    let mut current_view = view_type_menu;

  // Set to game first
  current_view = view_type_game;

  // Begin game loop
  let mut events = window.events();
  while let Some(event) = events.next(&mut window){
    match event {
      Render(args) => {
        match current_view {
          MENU(ref menu) => {
            menu.render(&args, &mut gl);
          },
          GAME(ref mut game) => {
            game.render(&args, &mut gl);
          },
          INSTRUCTIONS => {

          },
        };
      },
      AfterRender(args) => {

      },
      Update(args) => {

      },
      Idle(args) => {

      },
      Input(args) => {
        match current_view {
          MENU(ref menu) => {
          },
          GAME(ref mut game) => {
            match args {
              // todo : add Release(Button) with bool to disallow game.handle_input(key) from being
              // todo : called continuously by holding down the key.
              Press(button) => {
                match button {
                  Keyboard(key) => {
                    game.handle_input(key);
                  },
                  _ => {},
                };
              },
              _ => {},
            };
          },
          INSTRUCTIONS => {

          },
        };
      },
    }
/*
    // Rendering.
    if let Some(render_args) = event.render_args(){
      match current_view {
        MENU(ref menu) => {
          menu.render(&render_args, &mut gl);
        },
        GAME(ref mut game) => {
            game.render(&render_args, &mut gl);
        },
        INSTRUCTIONS => {

        },
      };
    }
    // After rendering.
    else if let Some(after_render_args) = event.after_render_args(){
      match current_view {
        MENU(ref menu) => {
        },
        GAME(ref game) => {

        },
        INSTRUCTIONS => {

        },
      };
    }
    // Idle.
    else if let Some(update_args) = event.idle_args(){
      match current_view {
        MENU(ref menu) => {
        },
        GAME(ref game) => {

        },
        INSTRUCTIONS => {

        },
      };
    }
    // Keyboard press.
    else if let Some(input_args) = event.press_args(){
      match current_view {
        MENU(ref menu) => {
        },
        GAME(ref game) => {
          match input_args {
            Keyboard(key) => {
              match key {
                Up => println!("Up"),
                Down => println!("Down"),
                Left => println!("Left"),
                Right => println!("Right"),
                _ => println!("Key not implemented"),
              }
            },
            _ => println!("Not keyboard"),
          }
        },
        INSTRUCTIONS => {

        },
      };
    }*/
  }
}