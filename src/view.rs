// Project libraries
use menu::Menu;
use game::Game;

/// GUI view types.
pub enum View {
    MENU(Menu),
    GAME(Game),
    INSTRUCTIONS,
}