// OpenGL library
use opengl_graphics::GlGraphics;
// Graphics library
use graphics::line::{Shape, Line};
use graphics::grid::Grid as Graphics_Grid;
use graphics::context::Context;
use graphics::{Transformed, rectangle};
// Project library
use color::Color;
use math::{Coordinates, Grid_Position};

/// Grid component.
pub struct Grid {
    /// Back-end graphics grid. Has cols, rows, and units (size of each cell).
    pub graphics_grid: Graphics_Grid,
    /// Coordinates of the grid on the window.
    pub coordinates: Coordinates,
    /// Color of the grid background.
    pub grid_background_color: Color,
    /// Color of the grid boarders.
    pub grid_color: Color,
    /// Size of the grid boarders.
    pub boarder_size: f64,
    /// Shape of the boarder edges.
    pub boarder_shape: Shape,
}
impl Grid {
    /// Creates a new grid without coordinates. This constructor will be used most of the time
    /// because the coordinates depend on the type of view, thus will have to be set in the view.
    pub fn new(cols:u32, rows:u32, cell_size: f64, grid_background_color: Color, grid_color: Color, boarder_size: f64, boarder_shape: Shape) -> Grid {
        Grid {
            // construct backend graphics grid.
            graphics_grid: Graphics_Grid {
                cols: cols,
                rows: rows,
                units: cell_size
            },
            coordinates: Coordinates::new(0.0, 0.0),
            grid_background_color: grid_background_color,
            grid_color: grid_color,
            boarder_size: boarder_size,
            boarder_shape: boarder_shape,
        }
    }

    /// Creates a new grid with coordinates.
    pub fn new_with_coordinates(cols:u32, rows:u32, cell_size:f64, coordinates: Coordinates, grid_background_color: Color, grid_color: Color, boarder_size:f64, boarder_shape: Shape) -> Grid{
        Grid {
            // construct backend graphics grid.
            graphics_grid: Graphics_Grid {
                cols: cols,
                rows: rows,
                units: cell_size
            },
            coordinates: coordinates,
            grid_background_color: grid_background_color,
            grid_color: grid_color,
            boarder_size: boarder_size,
            boarder_shape: boarder_shape,
        }
    }

    /// Draws the grid onto the screen.
    pub fn draw(&self, c: &Context, gl: &mut GlGraphics){
        // todo: Update, what if the grid is not a square?
        // Draw grid background.
        rectangle(self.grid_background_color.value(), rectangle::square(0.0, 0.0, (self.graphics_grid.units * self.graphics_grid.cols as f64)), c.trans(self.coordinates.x, self.coordinates.y).transform, gl);

        // Draw grid.
        match self.boarder_shape {
            Shape::Square => self.graphics_grid.draw(&Line::new(self.grid_color.value(), self.boarder_size), &Default::default(), c.trans(self.coordinates.x, self.coordinates.y).transform, gl),
            Shape::Round => self.graphics_grid.draw(&Line::new_round(self.grid_color.value(), self.boarder_size), &Default::default(), c.trans(self.coordinates.x, self.coordinates.y).transform, gl),
            Shape::Bevel => self.graphics_grid.draw(&Line::new(self.grid_color.value(), self.boarder_size), &Default::default(), c.trans(self.coordinates.x, self.coordinates.y).transform, gl),
        };
    }

    /// Set the coordinates of the grid.
    pub fn coordinates(&mut self, coordinates: Coordinates) -> &mut Grid{
        self.coordinates = coordinates;
        self
    }

    /// Finds the coordinates position of the specified cell index.
    pub fn cell_coordinates(&self, grid_position: Grid_Position) -> Coordinates{
        let x = self.coordinates.x + (grid_position.column as f64 * self.graphics_grid.units) + self.boarder_size;
        let y = self.coordinates.y + (grid_position.row as f64 * self.graphics_grid.units) + self.boarder_size;
        Coordinates::new(x, y)
    }
}