/// The representation of coordinates of an object.
pub struct Coordinates {
    pub x: f64,
    pub y: f64,
}
impl Coordinates {
    pub fn new(x: f64, y: f64) -> Coordinates{
        Coordinates {
            x: x,
            y: y,
        }
    }
}

/// The representation of the size of an object.
pub struct Size {
    pub width: f64,
    pub height: f64,
}
// todo : switch height and width around
impl Size {
    pub fn new(width: f64, height: f64) -> Size {
        Size {
            width: width,
            height: height,
        }
    }
}

/// The representation of a position on a grid.
pub struct Grid_Position {
    pub column: u32,
    pub row: u32,
}
impl Grid_Position {
    pub fn new(column: u32, row: u32) -> Grid_Position {
        Grid_Position {
            column: column,
            row: row,
        }
    }
}