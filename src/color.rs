/// Colors for entire app.
pub enum Color {
    /// Text
    BLACK,
    /// Background
    DARK_BROWN,
    /// Grid
    GREY_BROWN,
    /// Grid background
    LIGHT_GREY,
    /// 2 tile color
    LIGHT_BEIGE,
}

impl Color {
    /// Finds the [R,G,B, Opacity] values of the supplied color.
    pub fn value(&self) -> [f32; 4]{
        match *self {
            Color::BLACK => [0.0, 0.0, 0.0, 1.0],
            Color::DARK_BROWN => [0.16, 0.14, 0.01, 1.0],
            Color::GREY_BROWN => [0.52, 0.47, 0.42, 1.0],
            Color::LIGHT_GREY => [0.58, 0.54, 0.52, 1.0],
            Color::LIGHT_BEIGE => [0.93, 0.89, 0.87, 1.0],
        }
    }
}