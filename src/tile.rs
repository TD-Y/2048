// todo : Move all code to component.rs

// Project imports
use math::Coordinates;
use math::Size;
use color::Color;
// Piston/graphics/openlg_graphics imports
use opengl_graphics::GlGraphics;
use opengl_graphics::Texture;
use opengl_graphics::glyph_cache::GlyphCache;
use graphics::*;
// Rust imports
use std::path::Path;
use std::fmt;

/// 2048's Tiles.
pub struct Tile {
    /// Size of the tile.
    size: Size,
    /// Coordinates on the screen.
    coordinates: Coordinates,
    /// Color of the tile.
    color: Color,
    /// Value of the tile, 2^v where v is the value.
    value: u32,
}
impl Tile {
    /// Creates a new tile.
    pub fn new(size: Size, coordinates: Coordinates, color: Color, value: u32) -> Tile{
        Tile {
            size: size,
            coordinates: coordinates,
            color: color,
            value: value,
        }
    }

    /// Gets the value.
    pub fn value(&self) -> u32 {
        self.value
    }

    /// Draws the tile onto the window.
    pub fn draw(&self, font_cache:&mut GlyphCache,  c: &Context, gl: &mut GlGraphics) {
        rectangle(self.color.value(), rectangle::square(0.0, 0.0, self.size.width), c.trans(self.coordinates.x, self.coordinates.y).transform, gl);
        text(Color::BLACK.value(), self.size.width as u32, "2", font_cache, c.trans(self.coordinates.x + self.size.width/3.5, self.coordinates.y + self.size.width/1.2).transform, gl);
    }

    /// Updates an empty tile (value == 0) to the first tile (value == 1 (2)).
    pub fn update_empty(&mut self, coordinates: Coordinates) {
        self.coordinates = coordinates;
        self.color = Color::LIGHT_BEIGE;
        self.value = 1;
    }

    /// Updates the position of the tile on the grid.
    pub fn update_coordinates(&mut self, coordinates: Coordinates){
        self.coordinates = coordinates;
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}", self.value)
    }
}























