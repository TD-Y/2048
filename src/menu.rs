use piston::input::*;
use opengl_graphics::GlGraphics;
use component::Grid;
use color::Color;
use math::Coordinates;
use math::Size;
use graphics::*;
use tile::Tile;

//todo : Implement. Menu is in a very early stage, the code below is a fill in.
pub struct Menu {
}

impl Menu {

    pub fn new() -> Menu {
        Menu{}
    }

    /// Renders the menu to the screen
    pub fn render(&self, args: &RenderArgs, gl: &mut GlGraphics){
        gl.draw(args.viewport(), |c, gl|{
            clear(Color::DARK_BROWN.value(), gl);
            self.draw_title_grid(args, &c, gl);
        });
    }

    /// Constructs the title grid that surrounds the title.
    fn draw_title_grid(&self, args: &RenderArgs, c: &Context, gl: &mut GlGraphics){
        let mut grid = Grid::new(4, 4, 75.0, Color::LIGHT_GREY, Color::DARK_BROWN, 5.0, line::Shape::Round);

        //Calculate center position
        //Should be set up as 62.6 + 325 + 62.6 = 450
        //450 - (75 * 4 + 25) = 125/2 = 62.6
        let x_cord: f64 = (args.width as f64 - ((grid.graphics_grid.cols as f64 * grid.graphics_grid.units) + (grid.boarder_size * (grid.graphics_grid.cols + 1) as f64)))/2.0;
        let y_cord: f64 = (args.height/2) as f64;

        //Temporary 15, no idea why its not matching up...
        grid.coordinates(Coordinates::new(x_cord + 15 as f64, y_cord));
        grid.draw(c,gl);
    }
}