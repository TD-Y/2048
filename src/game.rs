// OpenGL Library
use opengl_graphics::GlGraphics;
use opengl_graphics::glyph_cache::GlyphCache;
// Graphics Library
use graphics::line::Shape::Round;
use graphics::{Context, clear};
// Project Library
use component::Grid;
use color::Color;
use math::{Coordinates, Size, Grid_Position};
use tile::Tile;
// Piston input
use piston::input::keyboard::Key;
use piston::input::keyboard::Key::{Up, Down, Left, Right};
use piston::input::{RenderArgs, UpdateArgs};
// Class lib
use lib_2048::{Grid as Game_Grid, Seed};
// Rust lib
use rand::IsaacRng;
use std::path::Path;

/// The Game view.
pub struct Game {
    /// The tiles of the 4x4 board.
    tiles: Vec<Tile>,
    /// The 16x16 graphics grid.
    grid: Grid,
    /// Random range.
    rng: IsaacRng,
    /// Font type.
    font: GlyphCache<'static>,
}
impl Game {

    /// Game view initializer.
    pub fn new(window_size: &Size, gl: &mut GlGraphics) -> Game {
        // Initialize Game
        let mut game = Game {
            tiles: Vec::<Tile>::with_capacity(16),
            grid: Grid::new(4, 4, 80.0, Color::LIGHT_GREY, Color::GREY_BROWN, 6.0, Round),
            rng: Seed::mk().rng(),
            font: GlyphCache::new(Path::new("assets/FiraSans-Regular.ttf")).unwrap(),
        };
        // Add empty tiles to all grid cells.
        let tile_size = game.grid.graphics_grid.units - (game.grid.boarder_size * 2.0);
        for index in 0..game.tiles.capacity() {
            game.tiles.push(Tile::new(Size::new(tile_size, tile_size), Coordinates::new(0.0, 0.0), Color::LIGHT_GREY, 0));
        }
        // Set the grid coordinates.
        let x: f64 = (window_size.width  - ((game.grid.graphics_grid.cols as f64 * game.grid.graphics_grid.units) + (game.grid.boarder_size * (game.grid.graphics_grid.cols + 1) as f64)))/2.0;
        let y: f64 = window_size.height/3.0;
        game.grid.coordinates(Coordinates::new(x + 13 as f64, y));
        // Spawn first two tiles
        game.spawn_tile();
        game.spawn_tile();
        game
    }

    /// Renders the game and its contents to the screen
    pub fn render(&mut self, args: &RenderArgs, gl: &mut GlGraphics){
        gl.draw(args.viewport(), |c, gl|{
            clear(Color::DARK_BROWN.value(), gl);
            self.draw_board(args, &c, gl);
        });
    }

    /// Handles the users input
    pub fn handle_input(&mut self, key: Key) {
        match key {
            Up => {
                println!("Up");
                for tile_index in 4..self.tiles.len(){
                    if self.tiles[tile_index].value() != 0 {
                        let grid_position = self.find_tile_column_row(tile_index as u32);
                        println!("tile_index : {} column : {}", tile_index, grid_position.row);
                        let row = grid_position.row + 1;
                        let mut tile_move_index = 16;
                        for row_index in 0..row {
                            println!("inner index : {}", row_index);
                            if self.tiles[(tile_index as u32 - (4 * row_index)) as usize].value() == 0 {
                                println!("Move to index : {}", tile_index as u32 - (4 * row_index));
                                tile_move_index = tile_index as u32 - (4 * row_index);
                            }
                        }
                        if tile_move_index != 16 {
                            self.tiles.swap(tile_move_index as usize, tile_index);
                            let coordinates = self.grid.cell_coordinates(self.find_tile_column_row(tile_move_index));
                            self.tiles[tile_move_index as usize].update_coordinates(coordinates);
                        }
                    }
                }
            },
            Down => {
                println!("Down");
                // todo : Use reverse loop to update bottom tiles first
            },
            Left => {
                println!("Left");
            },
            Right => {
                println!("Right");
                // todo : Use reverse loop to update right most tiles first
            },
            _ => {},
        };
    }

    /// Updates a tiles coordinates moving it either up, down, left, or right,
    /// depending on the users input.
    pub fn update_tile_coordinates(&self, args: &UpdateArgs){

    }

    /// Draws the board and its tiles
    fn draw_board(&mut self, args: &RenderArgs, c: &Context, gl: &mut GlGraphics){
        // Draw grid
        self.grid.draw(c, gl);
        // Draw existing tiles. Tiles with a value of 0 are non-existent.
        for index in 0..self.tiles.len(){
            if self.tiles[index].value() != 0 {
                self.tiles[index].draw(&mut self.font, c, gl);
            }
        }
    }

    /// Spawns a tile on the board. Returns false if cannot spawn anymore, thus game over.
    fn spawn_tile(&mut self) -> bool {
        // find empty cells
        let mut empty_cells = Vec::new();
        for index in 0..self.tiles.len(){
            if self.tiles[index].value() == 0 {
                empty_cells.push(index);
            }
        }
        // If there is at least one empty cell, continue with game
        if !empty_cells.is_empty(){
            // Find a random cell to spawn a tile.
            use rand::Rand;
            let index = empty_cells[(u8::rand(&mut self.rng) as usize) % empty_cells.len()];
            let coordinates = self.grid.cell_coordinates(self.find_tile_column_row(index as u32));
            self.tiles[index].update_empty(coordinates);
            return true
        }
        // No empty cells, game over!
        false
    }

    /// Finds the column and row of a tile with its provided index in the vec.d
    fn find_tile_column_row(&self, index: u32) -> Grid_Position{
        Grid_Position::new(index % 4, (index as f64/4.0).floor() as u32)
    }
}